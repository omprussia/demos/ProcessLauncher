// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef REQUESTHANDLER_H
#define REQUESTHANDLER_H

#include <QTimer>
#include <QCryptographicHash>
#include <QThread>

#include "httpServer/httpRequestHandler.h"
#include "httpServer/httpRequestRouter.h"

class RequestHandler : public HttpRequestHandler
{
private:
    HttpRequestRouter m_router;

    QString encodeToSha1(QString string);

public:
    explicit RequestHandler();
    void handle(HttpRequest *request, HttpResponse *response);

    void handleEncodeString(const QRegularExpressionMatch &match, HttpRequest *request,
                            HttpResponse *response);
};

#endif // REQUESTHANDLER_H
