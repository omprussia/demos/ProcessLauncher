// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "stringencoder.h"

StringEncoder::StringEncoder(QObject *parent) : QObject(parent)
{
    m_manager = new QNetworkAccessManager(this);
    m_reply = NULL;
    m_encoderProcess = new QProcess(this);
    m_encoderProcess->setProcessChannelMode(QProcess::MergedChannels);
    connect(m_encoderProcess, &QProcess::errorOccurred, this, &StringEncoder::processErrorOccurred);
    connect(m_encoderProcess, &QProcess::started, this, &StringEncoder::processStarted);
}

StringEncoder::~StringEncoder()
{
    stop();
    delete m_encoderProcess;
    delete m_manager;
}

void StringEncoder::start()
{
    m_stopping = false;
    m_encoderProcess->start("/usr/libexec/ru.auroraos.ProcessLauncher/encoding_server");
}

void StringEncoder::sendToEncode(QString str)
{
    qDebug() << "Send string to encode:" << str;
    this->m_strToEncode = str;
    emit statusChanged(StringEncoderStatus::Running);
    QNetworkRequest request;
    request.setUrl(QUrl("http://localhost:44387/encode"));
    request.setRawHeader("Content-type", "application/json");
    request.setRawHeader("Accept", "application/json");

    QJsonObject obj;
    obj["toEncode"] = m_strToEncode;
    QJsonDocument doc(obj);
    QByteArray data = doc.toJson();

    m_reply = m_manager->post(request, data);

    QObject::connect(m_reply, &QNetworkReply::finished, [=]() {
        if (m_reply->error() == QNetworkReply::NoError) {
            QByteArray replyData = m_reply->readAll();
            QString contents = QString::fromUtf8(replyData);
            emit statusChanged(StringEncoderStatus::Done);
            qDebug() << "Server answer:" << contents;
            QJsonDocument jdata = QJsonDocument::fromJson(replyData);
            emit encoded(jdata.object()["encoded"].toString());
        } else {
            QString err = m_reply->errorString();
            emit statusChanged(StringEncoderStatus::Error);
            qDebug() << err;
        }
        m_reply->deleteLater();
        m_reply = NULL;
    });
}

void StringEncoder::stop()
{
    qDebug() << "Process stopped";

    if (m_reply != NULL) {
        m_reply->disconnect();
        delete m_reply;
        m_reply = NULL;
    }

    m_stopping = true;
    m_encoderProcess->kill();
    emit statusChanged(StringEncoderStatus::NotRunning);
}

void StringEncoder::processErrorOccurred(QProcess::ProcessError err)
{
    if (!m_stopping) {
        qDebug() << "processErrorOccurred" << err;
        emit statusChanged(StringEncoderStatus::Error);
    }
}

void StringEncoder::processStarted()
{
    qDebug() << "Process started";
    emit statusChanged(StringEncoderStatus::Starting);
}
