Name:       ru.auroraos.ProcessLauncher
Summary:    Приложение, которое демонстрирует запуск бинарных модуля через QProcess
Version:    0.1
Release:    1
License:    BSD-3-Clause
URL:        https://auroraos.ru
Source0:    %{name}-%{version}.tar.bz2

Requires:   sailfishsilica-qt5 >= 0.10.9
BuildRequires:  pkgconfig(auroraapp)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  cmake
BuildRequires:  zlib

%define __provides_exclude_from ^%{_datadir}/.*$
%define __requires_exclude ^(libHttpServer.*).*$

%description
Приложение, которое демонстрирует запуск бинарных модуля через QProcess

%prep
%autosetup

%build
%cmake
%make_build

%install
%make_install
mv %{buildroot}/%{_libdir}/ %{buildroot}/%{_datadir}/%{name}/lib/

%files
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png

%defattr(-,root,root,-)
%{_libexecdir}/%{name}/image_compressor
%{_libexecdir}/%{name}/encoding_server
