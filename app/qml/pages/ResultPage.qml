// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    property string url

    backgroundColor: "white"

    Image {
        anchors.centerIn: parent
        width: parent.width - Theme.paddingLarge
        height: 1.0 * sourceSize.height / sourceSize.width * width
        source: url
    }

    Component.onCompleted: console.log(url)
}
