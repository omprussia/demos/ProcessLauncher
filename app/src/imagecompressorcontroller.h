// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PROCESSCONTROLLER_H
#define PROCESSCONTROLLER_H

#include <QObject>
#include <QProcess>
#include <QtDebug>

class ImageCompressorController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString compressedImage READ compressedImage NOTIFY compressedImageChanged)
    Q_PROPERTY(QString image READ image)
public:
    explicit ImageCompressorController(QObject *parent = nullptr);
    Q_INVOKABLE void compressImage(QString path);
    Q_INVOKABLE void stopCompression();
    Q_INVOKABLE void startCompression();
    Q_INVOKABLE QString compressedImage() const;
    Q_INVOKABLE QString image() const;
    Q_INVOKABLE QString cutPath(QString path) const;

signals:
    void compressedImageChanged();

private:
    QProcess *m_compressProcess;
    QString m_imagePath;
    QString m_compressedImagePath;
};

#endif // PROCESSCONTROLLER_H
