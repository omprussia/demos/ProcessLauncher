// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "imagecompressorcontroller.h"

using namespace std;

ImageCompressorController::ImageCompressorController(QObject *parent) : QObject(parent)
{
    m_compressProcess = NULL;
}

void ImageCompressorController::compressImage(QString path)
{
    m_imagePath = path;
    startCompression();
}

void ImageCompressorController::stopCompression()
{
    if (m_compressProcess != NULL) {
        m_compressProcess->close();
        m_compressProcess->deleteLater();
        m_compressProcess = NULL;
    }
}

void ImageCompressorController::startCompression()
{
    m_compressProcess = new QProcess(this);
    m_compressProcess->start("/usr/libexec/ru.auroraos.ProcessLauncher/image_compressor");
    if (!m_compressProcess->waitForStarted()) {
        qDebug() << "Process launching error";
        return;
    }

    qDebug() << "Compression started";

    QObject::connect(m_compressProcess, &QProcess::readyReadStandardOutput,
                     [=, controller = this]() {
                         controller->m_compressedImagePath = m_compressProcess->readAll();
                         qDebug() << "Compressed image: " << controller->m_compressedImagePath;
                         emit compressedImageChanged();
                         m_compressProcess->close();
                         m_compressProcess->deleteLater();
                         m_compressProcess = NULL;
                     });

    qDebug() << "Image to compress: " << m_imagePath.toStdString().data();
    m_compressProcess->write(m_imagePath.toStdString().data());
    m_compressProcess->write("\n");
    m_compressProcess->waitForBytesWritten();
}

QString ImageCompressorController::compressedImage() const
{
    return m_compressedImagePath;
}

QString ImageCompressorController::image() const
{
    return m_imagePath;
}

QString ImageCompressorController::cutPath(QString path) const
{
    QStringList splittedPath = path.split("/");
    splittedPath.pop_front();
    splittedPath.pop_front();
    splittedPath[0] = "~";
    return splittedPath.join("/");
}
