// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef STRINGENCODER_H
#define STRINGENCODER_H

#include <QObject>
#include <QProcess>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QDebug>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>

class StringEncoder : public QObject
{
    Q_OBJECT
public:
    enum StringEncoderStatus { NotRunning, Running, Starting, Error, Done };
    Q_ENUM(StringEncoderStatus)

    explicit StringEncoder(QObject *parent = nullptr);
    ~StringEncoder();
    void start();
    void sendToEncode(QString str);
    void stop();

private:
    QString m_strToEncode;
    QProcess *m_encoderProcess;
    QNetworkAccessManager *m_manager;
    QNetworkReply *m_reply;
    bool m_stopping;

private slots:
    void processErrorOccurred(QProcess::ProcessError err);
    void processStarted();
signals:
    void statusChanged(StringEncoderStatus status);
    void encoded(QString encodedStr);
};

#endif // STRINGENCODER_H
