// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "textencoderpagecontroller.h"

TextEncoderPageController::TextEncoderPageController(QObject *parent) : QObject(parent)
{
    connect(&m_encoder, &StringEncoder::statusChanged, this,
            &TextEncoderPageController::statusChanged);
    connect(&m_encoder, &StringEncoder::encoded, this, &TextEncoderPageController::encoded);
}

void TextEncoderPageController::encode(QString strToEncode)
{
    m_encoder.sendToEncode(strToEncode);
}

void TextEncoderPageController::start()
{
    m_encoder.start();
}

void TextEncoderPageController::stop()
{
    m_encoder.stop();
}

void TextEncoderPageController::statusChanged(StringEncoder::StringEncoderStatus newStatus)
{
    emit updateStatus(QVariant::fromValue(newStatus).toString());
}
