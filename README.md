# Process Launcher

The application demonstrates the launch of binary modules via QProcess

## Process scenarios

The application implements two scenarios to show different ways of interaction with processes.

### String encoding process

A process that is running in the background with data transmission over a socket.
The user can start and stop a process that is a socket server.
The server receives an http request with a string to encode, then the server encodes it, falls in sleep for 10 seconds and sends the encoded string as a response.

### Image compression process

A process that performs a single task with interaction via stdout.
The user can start a process that gets the path to the image to compress via stdin.
After compression, the process sends the path to the compressed image via stdout and turns off.
The application takes the image path from stdout and shows it to the user.

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-CLAUSE.md), which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile Platform which informs you how we expect the members of the community will interact while contributing and communicating.

## Project Structure

The project has a standard structure of an application based on C++ and QML for Aurora OS.

* **[app](app)** directory contains the application source code:
  * **[CMakeLists.txt](app/CMakeLists.txt)** file describes the app subproject structure for the CMake build system.
  * **[icons](app/icons)** directory contains the application icons for different screen resolutions.
  * **[qml](app/qml)** directory contains the QML source code and the UI resources.
    * **[cover](app/qml/cover)** directory contains the application cover implementations.
    * **[icons](app/qml/icons)** directory contains the additional custom UI icons.
    * **[pages](app/qml/pages)** directory contains the application pages.
      * **[MainPage.qml](app/qml/pages/MainPage.qml)** file describes the initial page of the application, which contains buttons to run a scenario of interacting with processes.
      * **[ImageCompressionPage.qml](app/qml/pages/ImageCompressionPage.qml)** file describes the page for managing the image compression process.
      * **[StringEncoderPage.qml](app/qml/pages/StringEncoderPage.qml)** file describes the page for managing the string encoding process.
    * **[ProcessLauncher.qml](app/qml/ProcessLauncher.qml)** file provides the application window implementation.
  * **[src](app/src)** directory contains the C++ source code.
    * **[main.cpp](app/src/main.cpp)** file is the application entry point.
  * **[translations](app/translations)** directory contains the UI translation files.
* **[encoding_server](encoding_server)** directory contains the string encoding server subproject:
  * **[CMakeLists.txt](encoding_server/CMakeLists.txt)** file describes the string encoding server subproject structure for the CMake build system.
  * **[src](encoding_server/src)** directory contains the C++ source code.
* **[image_compressor](image_compressor)** directory contains the image compressor subproject:
  * **[CMakeLists.txt](image_compressor/CMakeLists.txt)** file describes the image compressor subproject structure for the CMake build system.
  * **[src](image_compressor/src)** directory contains the C++ source code.
* **[3rdparty](3rdparty)** directory contains 3rdparty libraries subproject:
  * **[CMakeLists.txt](3rdparty/CMakeLists.txt)** file describes 3rdparty libraries subproject structure for the CMake build system.
  * **[HttpServer](3rdparty/HttpServer)** directory contains [HttpServer](https://github.com/addisonElliott/HttpServer) library source code.
  * **[k-means-compressor](3rdparty/k-means-compressor)** directory contains [k-means-compressor](https://github.com/DebadityaPal/k-means-compressor) library source code.
* **[ru.auroraos.ProcessLauncher.desktop](ru.auroraos.ProcessLauncher.desktop)** file defines the display and parameters for launching the application.
* **[CMakeLists.txt](CMakeLists.txt)** file describes the project structure for the CMake build system.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.ProcessLauncher.spec](rpm/ru.auroraos.ProcessLauncher.spec)** file is used by rpmbuild tool.

## Compatibility

The project is compatible with all the current versions of the Aurora OS.

## Usage

On the initial page of the application, select "Encode a string" or "Compress image process" to go to the page of the required process.

### String encoding process

1. Start the process.
2. Enter the string you want to encode.
3. Click "Encode" and wait until the encoded string is displayed under the control buttons.
You can stop the process at any time, then the encoding will be interrupted.
When the string encoding page is closed, the process is stopped.

### Image compression process

Select the image you want to compress. After selecting it, the compression process will start immediately.
The process can be stopped during execution. If the process has been stopped, it can be started again with the same image.
When the compression process is completed, a file with a compressed image will be created in the file system. You can see the compressed image by clicking the "See result" button.

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
