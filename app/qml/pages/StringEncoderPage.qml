// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.auroraos.TextEncoderPageController 1.0

Page {
    Column {
        width: parent.width
        spacing: Theme.paddingLarge

        PageHeader {
            title: qsTr("Encode a string")
        }

        TextEncoderPageController {
            id: controller

            onEncoded: result.text = encodedStr

            onUpdateStatus: status.state = newStatus
        }

        Label {
            id: status

            anchors.horizontalCenter: parent.horizontalCenter
            state: "NotRunning"

            states: [
                State {
                    name: "NotRunning"
                    PropertyChanges {
                        target: status
                        text: qsTr("Status: Not running")
                        color: "black"
                    }
                    PropertyChanges {
                        target: encodeBtn
                        enabled: false
                    }
                    PropertyChanges {
                        target: controllBtn
                        text: qsTr("Start process")

                        onClicked: controller.start()
                    }
                },
                State {
                    name: "Running"
                    PropertyChanges {
                        target: status
                        text: qsTr("Status: Running")
                        color: "orange"
                    }
                    PropertyChanges {
                        target: encodeBtn
                        enabled: false
                    }
                    PropertyChanges {
                        target: controllBtn
                        text: qsTr("Stop process")

                        onClicked: controller.stop()
                    }
                },
                State {
                    name: "Starting"
                    PropertyChanges {
                        target: status
                        text: qsTr("Status: Ready to work")
                        color: "black"
                    }
                    PropertyChanges {
                        target: encodeBtn
                        enabled: true
                    }
                    PropertyChanges {
                        target: controllBtn
                        text: qsTr("Stop process")

                        onClicked: controller.stop()
                    }
                },
                State {
                    name: "Error"
                    PropertyChanges {
                        target: status
                        text: qsTr("Status: Error")
                        color: "red"
                    }
                    PropertyChanges {
                        target: encodeBtn
                        enabled: false
                    }
                    PropertyChanges {
                        target: controllBtn
                        text: qsTr("Start process")

                        onClicked: controller.start()
                    }
                },
                State {
                    name: "Done"
                    PropertyChanges {
                        target: status
                        text: qsTr("Status: Done")
                        color: "green"
                    }
                    PropertyChanges {
                        target: encodeBtn
                        enabled: true
                    }
                    PropertyChanges {
                        target: controllBtn
                        text: qsTr("Stop process")

                        onClicked: controller.stop()
                    }
                }
            ]
        }

        TextField {
            id: textToEncode

            placeholderText: qsTr("Text to encode")
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Button {
            id: encodeBtn

            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Encode")
            width: Theme.buttonWidthLarge

            onClicked: {
                console.log("to encode: " + textToEncode.text);
                result.text = "";
                controller.encode(textToEncode.text);
            }
        }

        Button {
            id: controllBtn

            anchors.horizontalCenter: parent.horizontalCenter
            width: Theme.buttonWidthLarge
        }

        Label {
            id: outputHint

            text: qsTr("Encoded string:")
            color: "darkGray"
            visible: result.text.length !== 0
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
            id: result

            width: parent.width - 3 * Theme.paddingLarge
            anchors.horizontalCenter: parent.horizontalCenter
            text: ""
            wrapMode: Text.Wrap
        }
    }
}
