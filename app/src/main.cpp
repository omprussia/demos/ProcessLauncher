// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <auroraapp.h>
#include <QtQuick>

#include "textencoderpagecontroller.h"
#include "imagecompressorcontroller.h"

int main(int argc, char *argv[])
{
    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("ProcessLauncher"));

    qmlRegisterType<ImageCompressorController>("ru.auroraos.ImageCompressorController", 1, 0,
                                               "ImageCompressorController");
    qmlRegisterType<TextEncoderPageController>("ru.auroraos.TextEncoderPageController", 1, 0,
                                               "TextEncoderPageController");

    QScopedPointer<QQuickView> view(Aurora::Application::createView());
    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/ProcessLauncher.qml")));
    view->show();

    return application->exec();
}
