// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QCoreApplication>
#include <iostream>

#include "httpServer/httpServer.h"
#include "requesthandler.h"

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    HttpServerConfig config;
    config.port = 44387;
    config.requestTimeout = 20;
    config.verbosity = HttpServerConfig::Verbose::All;
    config.maxMultipartSize = 512 * 1024 * 1024;

    RequestHandler *handler = new RequestHandler();
    HttpServer *server = new HttpServer(config, handler);
    server->listen();

    return a.exec();
}
