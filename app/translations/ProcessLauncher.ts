<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="26"/>
        <source>About Application</source>
        <translation>About Application</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="36"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;Приложение, которое демонстрирует запуск бинарных модуля через QProcess&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>3-Clause BSD License</source>
        <translation>3-Clause&#xa0;BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright&#xa0;(C)&#xa0;2024 ru.auroraos&lt;/i&gt;&lt;/p&gt;
&lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
&lt;ol&gt;
        &lt;li&gt;Redistributions of&#xa0;source code must retain the above copyright notice, this list of&#xa0;conditions and the following disclaimer.&lt;/li&gt;
        &lt;li&gt;Redistributions in&#xa0;binary form must reproduce the above copyright notice, this list of&#xa0;conditions and the following disclaimer in&#xa0;the documentation and/or other materials provided with the distribution.&lt;/li&gt;
        &lt;li&gt;Neither the name of&#xa0;the copyright holder nor the names of&#xa0;its contributors may be&#xa0;used to&#xa0;endorse or&#xa0;promote products derived from this software without specific prior written permission.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;THIS SOFTWARE IS&#xa0;PROVIDED BY&#xa0;THE&#xa0;COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS&#xa0;IS&quot; AND ANY EXPRESS OR&#xa0;IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE&#xa0;IMPLIED WARRANTIES OF&#xa0;MERCHANTABILITY AND FITNESS FOR A&#xa0;PARTICULAR PURPOSE ARE DISCLAIMED. IN&#xa0;NO&#xa0;EVENT SHALL THE&#xa0;COPYRIGHT HOLDER OR&#xa0;CONTRIBUTORS BE&#xa0;LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR&#xa0;CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF&#xa0;SUBSTITUTE GOODS OR&#xa0;SERVICES; LOSS OF&#xa0;USE, DATA, OR&#xa0;PROFITS; OR&#xa0;BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON&#xa0;ANY THEORY OF&#xa0;LIABILITY, WHETHER IN&#xa0;CONTRACT, STRICT LIABILITY, OR&#xa0;TORT (INCLUDING NEGLIGENCE OR&#xa0;OTHERWISE) ARISING IN&#xa0;ANY WAY OUT OF&#xa0;THE&#xa0;USE OF&#xa0;THIS SOFTWARE, EVEN IF&#xa0;ADVISED OF&#xa0;THE&#xa0;POSSIBILITY OF&#xa0;SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="12"/>
        <source>Process Launcher</source>
        <translation>Process Launcher</translation>
    </message>
</context>
<context>
    <name>ImageCompressionPage</name>
    <message>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="18"/>
        <source>Process is not running</source>
        <translation>Process is not running</translation>
    </message>
    <message>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="23"/>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="56"/>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="72"/>
        <source>Start process</source>
        <translation>Start process</translation>
    </message>
    <message>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="31"/>
        <source>Process is running</source>
        <translation>Process is running</translation>
    </message>
    <message>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="36"/>
        <source>Stop process</source>
        <translation>Stop process</translation>
    </message>
    <message>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="51"/>
        <source>Process is stopped</source>
        <translation>Process is stopped</translation>
    </message>
    <message>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="67"/>
        <source>Process is finished</source>
        <translation>Process is finished</translation>
    </message>
    <message>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="85"/>
        <source>Image compression page</source>
        <translation>Image compression page</translation>
    </message>
    <message>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="111"/>
        <source>Image to compress:</source>
        <translation>Image to compress:</translation>
    </message>
    <message>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="135"/>
        <source>Select image</source>
        <translation>Select image</translation>
    </message>
    <message>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="155"/>
        <source>See result</source>
        <translation>See result</translation>
    </message>
    <message>
        <location filename="../qml/cover/ImageCompressionPage.qml" line="166"/>
        <source>Compressed image:</source>
        <translation>Compressed image:</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="11"/>
        <source>Process Launcher</source>
        <translation>Process Launcher</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="36"/>
        <source>Encode a string</source>
        <translation>Encode a string</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="43"/>
        <source>A process running in the background with data transmission over a socket</source>
        <translation>A process running in the background with data transmission over a socket</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="58"/>
        <source>Compress image process</source>
        <translation>Compress image process</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="65"/>
        <source>A process that performs a single task with interaction via stdout</source>
        <translation>A process that performs a single task with interaction via stdout</translation>
    </message>
</context>
<context>
    <name>StringEncoderPage</name>
    <message>
        <location filename="../qml/cover/StringEncoderPage.qml" line="13"/>
        <source>Encode a string</source>
        <translation>Encode a string</translation>
    </message>
    <message>
        <location filename="../qml/cover/StringEncoderPage.qml" line="35"/>
        <source>Status: Not running</source>
        <translation>Status: Not running</translation>
    </message>
    <message>
        <location filename="../qml/cover/StringEncoderPage.qml" line="44"/>
        <location filename="../qml/cover/StringEncoderPage.qml" line="98"/>
        <source>Start process</source>
        <translation>Start process</translation>
    </message>
    <message>
        <location filename="../qml/cover/StringEncoderPage.qml" line="53"/>
        <source>Status: Running</source>
        <translation>Status: Running</translation>
    </message>
    <message>
        <location filename="../qml/cover/StringEncoderPage.qml" line="62"/>
        <location filename="../qml/cover/StringEncoderPage.qml" line="80"/>
        <location filename="../qml/cover/StringEncoderPage.qml" line="116"/>
        <source>Stop process</source>
        <translation>Stop process</translation>
    </message>
    <message>
        <location filename="../qml/cover/StringEncoderPage.qml" line="71"/>
        <source>Status: Ready to work</source>
        <translation>Status: Ready to work</translation>
    </message>
    <message>
        <location filename="../qml/cover/StringEncoderPage.qml" line="89"/>
        <source>Status: Error</source>
        <translation>Status: Error</translation>
    </message>
    <message>
        <location filename="../qml/cover/StringEncoderPage.qml" line="107"/>
        <source>Status: Done</source>
        <translation>Status: Done</translation>
    </message>
    <message>
        <location filename="../qml/cover/StringEncoderPage.qml" line="127"/>
        <source>Text to encode</source>
        <translation>Text to encode</translation>
    </message>
    <message>
        <location filename="../qml/cover/StringEncoderPage.qml" line="135"/>
        <source>Encode</source>
        <translation>Encode</translation>
    </message>
    <message>
        <location filename="../qml/cover/StringEncoderPage.qml" line="155"/>
        <source>Encoded string:</source>
        <translation>Encoded string:</translation>
    </message>
</context>
</TS>
