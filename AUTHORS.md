# Authors

* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2024
* Konstantin Zvyagin, <k.zvyagin@omp.ru>  
  * Maintainer, 2024
* Vladislav Larionov
  * Developer, 2023-2024
* Alexander Lukyanov
  * Developer, 2023-2024
