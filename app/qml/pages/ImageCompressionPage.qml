// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import ru.auroraos.ImageCompressorController 1.0

Page {
    id: page

    state: "notRunning"

    states: [
        State {
            name: "notRunning"
            PropertyChanges {
                target: processStatus
                text: qsTr("Process is not running")
                color: "black"
            }
            PropertyChanges {
                target: controllBtn
                text: qsTr("Start process")
                enabled: false
            }
        },
        State {
            name: "running"
            PropertyChanges {
                target: processStatus
                text: qsTr("Process is running")
                color: "orange"
            }
            PropertyChanges {
                target: controllBtn
                text: qsTr("Stop process")
                onClicked: {
                    imageCompressorController.stopCompression();
                    page.state = "stopped";
                }
            }
            PropertyChanges {
                target: selectBtn
                enabled: false
            }
        },
        State {
            name: "stopped"
            PropertyChanges {
                target: processStatus
                text: qsTr("Process is stopped")
                color: "red"
            }
            PropertyChanges {
                target: controllBtn
                text: qsTr("Start process")
                onClicked: {
                    imageCompressorController.startCompression();
                    page.state = "running";
                }
            }
        },
        State {
            name: "finished"
            PropertyChanges {
                target: processStatus
                text: qsTr("Process is finished")
                color: "green"
            }
            PropertyChanges {
                target: controllBtn
                text: qsTr("Start process")
                enabled: false
            }
            PropertyChanges {
                target: seeImageBtn
                enabled: true
            }
        }
    ]

    PageHeader {
        id: pageHeader

        title: qsTr("Image compression page")
    }

    Label {
        id: processStatus

        anchors {
            horizontalCenter: parent.horizontalCenter
            top: pageHeader.bottom
            topMargin: Theme.paddingLarge
        }
    }

    Column {
        spacing: Theme.paddingLarge

        anchors {
            left: parent.left
            right: parent.right
            top: processStatus.bottom
            topMargin: Theme.paddingLarge
        }

        Label {
            id: imgPathHint

            text: qsTr("Image to compress:")
            color: "darkGray"
            visible: false
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Label {
            id: imgPath

            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap

            anchors {
                left: parent.left
                leftMargin: 1.5 * Theme.paddingLarge
                right: parent.right
                rightMargin: 1.5 * Theme.paddingLarge
            }
        }

        Button {
            id: selectBtn

            width: Theme.buttonWidthLarge
            text: qsTr("Select image")
            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: pageStack.push(imagePickerComponent)
        }

        Button {
            id: controllBtn

            width: Theme.buttonWidthLarge
            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: pageStack.push(imagePickerComponent)
        }

        Button {
            id: seeImageBtn

            enabled: false
            width: Theme.buttonWidthLarge
            text: qsTr("See result")
            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: pageStack.push(Qt.resolvedUrl("ResultPage.qml"), {
                    "url": imageCompressorController.compressedImage
                })
        }

        Label {
            id: outputHint

            text: qsTr("Compressed image:")
            color: "darkGray"
            visible: false
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Label {
            id: output

            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap

            anchors {
                left: parent.left
                leftMargin: 1.5 * Theme.paddingLarge
                right: parent.right
                rightMargin: 1.5 * Theme.paddingLarge
            }
        }
    }

    ImageCompressorController {
        id: imageCompressorController

        onCompressedImageChanged: {
            output.text = cutPath(compressedImage);
            outputHint.visible = true;
            page.state = "finished";
        }
    }

    Component {
        id: imagePickerComponent

        ImagePickerPage {
            onSelectedContentChanged: {
                imageCompressorController.compressImage(selectedContentProperties.filePath);
                imgPath.text = imageCompressorController.cutPath(imageCompressorController.image);
                imgPathHint.visible = true;
                page.state = "running";
                output.text = "";
                outputHint.visible = false;
            }
        }
    }

    Component.onDestruction: imageCompressorController.stopCompression()
}
