# SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

cmake_minimum_required(VERSION 3.5)

project(image_compressor)

set(CMAKE_VERBOSE_MAKEFILE ON)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_SKIP_BUILD_RPATH FALSE)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)
set(CMAKE_INSTALL_RPATH "/usr/share/ru.auroraos.ProcessLauncher/lib")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH FALSE)

include(GNUInstallDirs)

add_executable(${PROJECT_NAME}
    src/imagecompressor.cpp
)

target_include_directories(${PROJECT_NAME} PUBLIC
     ${CMAKE_SOURCE_DIR}/3rdparty/k-means-compressor/src
     "${PROJECT_BINARY_DIR}"
)

install(TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION ${CMAKE_INSTALL_LIBEXECDIR}/ru.auroraos.ProcessLauncher/
)
