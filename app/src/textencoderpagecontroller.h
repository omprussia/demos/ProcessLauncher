// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef TEXTENCODERPAGECONTROLLER_H
#define TEXTENCODERPAGECONTROLLER_H

#include <QObject>
#include <QVariant>

#include "stringencoder.h"

class TextEncoderPageController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(void encoded NOTIFY encoded)
    Q_PROPERTY(void updateStatus NOTIFY updateStatus)
public:
    explicit TextEncoderPageController(QObject *parent = nullptr);

public slots:
    void encode(QString strToEncode);
    void start();
    void stop();
    void statusChanged(StringEncoder::StringEncoderStatus newStatus);
signals:
    void encoded(QString encodedStr);
    void updateStatus(QString newStatus);

private:
    StringEncoder m_encoder;
};

#endif // TEXTENCODERPAGECONTROLLER_H
