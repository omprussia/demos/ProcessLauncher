// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "requesthandler.h"

RequestHandler::RequestHandler()
{
    m_router.addRoute("POST", "^/encode/?$", this, &RequestHandler::handleEncodeString);
}

void RequestHandler::handle(HttpRequest *request, HttpResponse *response)
{
    // If this is handled another way, then do nothing
    if (m_router.route(request, response))
        return;

    if (request->mimeType().compare("application/json", Qt::CaseInsensitive) != 0)
        return response->setError(HttpStatus::BadRequest,
                                  "Request body content type must be application/json");

    QJsonDocument jsonDocument = request->parseJsonBody();
    if (jsonDocument.isNull())
        return response->setError(HttpStatus::BadRequest, "Invalid JSON body");

    QJsonObject object;
    object["test"] = 5;
    object["another test"] = "OK";

    response->setStatus(HttpStatus::Ok, QJsonDocument(object));
}

void RequestHandler::handleEncodeString(const QRegularExpressionMatch &match, HttpRequest *request,
                                        HttpResponse *response)
{
    QJsonDocument jsonDocument = request->parseJsonBody();
    QJsonObject body = jsonDocument.object();
    qInfo() << "Str to encode:" << body["toEncode"].toString();
    QString encodedStr = encodeToSha1(body["toEncode"].toString());
    QJsonObject respBody;
    respBody["encoded"] = encodedStr;

    sleep(10);

    response->setStatus(HttpStatus::Ok, QJsonDocument(respBody));
}

QString RequestHandler::encodeToSha1(QString string)
{
    return QString(
            QCryptographicHash::hash(string.toLocal8Bit(), QCryptographicHash::Sha1).toHex());
}
