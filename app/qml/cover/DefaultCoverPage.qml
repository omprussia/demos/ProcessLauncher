// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    objectName: "defaultCover"

    CoverPlaceholder {
        objectName: "placeholder"

        text: qsTr("Process Launcher")
        forceFit: true

        icon {
            source: Qt.resolvedUrl("../icons/ProcessLauncher.svg")

            sourceSize {
                width: Theme.iconSizeExtraLarge
                height: Theme.iconSizeExtraLarge
            }
        }
    }
}
