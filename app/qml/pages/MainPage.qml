// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    PageHeader {
        id: header
        objectName: "pageHeader"

        title: qsTr("Process Launcher")

        extraContent.children: [
            IconButton {
                objectName: "aboutButton"

                icon.source: "image://theme/icon-m-about"
                anchors.verticalCenter: parent.verticalCenter

                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
        ]
    }

    Column {
        width: parent.width
        spacing: 4 * Theme.paddingLarge

        anchors {
            top: header.bottom
            topMargin: 2 * Theme.paddingLarge
        }

        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Encode a string")
            width: Theme.buttonWidthLarge

            onClicked: pageStack.push(Qt.resolvedUrl("StringEncoderPage.qml"))

            Label {
                color: "gray"
                text: qsTr("A process running in the background with data transmission over a socket")
                font.pixelSize: Theme.fontSizeExtraSmall
                wrapMode: Text.Wrap

                anchors {
                    top: parent.bottom
                    topMargin: Theme.paddingSmall
                    right: parent.right
                    left: parent.left
                }
            }
        }

        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Compress image process")
            width: Theme.buttonWidthLarge

            onClicked: pageStack.push(Qt.resolvedUrl("ImageCompressionPage.qml"))

            Label {
                color: "gray"
                text: qsTr("A process that performs a single task with interaction via stdout")
                font.pixelSize: Theme.fontSizeExtraSmall
                wrapMode: Text.Wrap

                anchors {
                    top: parent.bottom
                    topMargin: Theme.paddingSmall
                    right: parent.right
                    left: parent.left
                }
            }
        }
    }
}
