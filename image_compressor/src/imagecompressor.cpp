// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include <chrono>
#include "ImageRead.hpp"
#include "ImageWrite.hpp"
#include "KMeans.hpp"

using namespace std;

int main(int argc, char *argv[])
{
    string url;
    cin >> url;

    size_t dotPos = url.find_last_of(".");
    string compressedUrl = url.substr(0, dotPos);
    compressedUrl.append("-compressed");
    compressedUrl.append(url.substr(dotPos, url.length()));

    ImageReader imread((char *)url.c_str(), 3);
    KMeans compressor(8, imread.matrix, imread.height, imread.width, imread.channels);
    compressor.fit(5);
    ImageWriter writer((char *)compressedUrl.c_str(), imread.width, imread.height, imread.channels,
                       imread.matrix);
    writer.save();

    cout << compressedUrl;
}
